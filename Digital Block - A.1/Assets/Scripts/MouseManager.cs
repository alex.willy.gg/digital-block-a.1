﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class MouseManager : MonoBehaviour
{
    //What objects are clickable
    public LayerMask clickableLayer;

    //Swap Cursor per object
    public Texture2D pointer; //Normal Pointer
    public Texture2D target; //Cursor for clickable objects like the world
    public Texture2D doorway; //Cursor for dooways
    public Texture2D combat; //Cursor for combat actions
    public Texture2D grab; //Cursor to grab items
    public EventVector3 onClickEnviroment;

    // Update is called once per frame
    void Update()
    {
        RaycastHit hit;

        if(Physics.Raycast(Camera.main.ScreenPointToRay(Input.mousePosition), out hit, 50, clickableLayer.value)) 
        {
            bool door = false;
            bool item = false;
            bool enemy = false;

            if (hit.collider.gameObject.tag == "Doorway")
            {
                Cursor.SetCursor(doorway, new Vector2(16, 16), CursorMode.Auto);
                door = true;
            }
            else if (hit.collider.gameObject.tag == "Item") 
            {
                Cursor.SetCursor(grab, new Vector2(16, 16), CursorMode.Auto);
                item = true;
            }
            else
                {
                if (hit.collider.gameObject.tag == "Enemy")
                {
                    Cursor.SetCursor(combat, new Vector2(16, 16), CursorMode.Auto);
                    enemy = true;
                }
                else
                {
                    Cursor.SetCursor(target, new Vector2(16, 16), CursorMode.Auto);
                }    
            }

            if (Input.GetMouseButtonDown(0))
            {
                if (door)
                {
                    Transform doorway = hit.collider.gameObject.transform;

                    onClickEnviroment.Invoke(doorway.position);
                    Debug.Log("DOOR");
                }
                else if(item)
                {
                    Transform itemPos = hit.collider.gameObject.transform;

                    onClickEnviroment.Invoke(itemPos.position);
                    Debug.Log("ITEM");
                }
                else
                {
                    if (enemy)
                    {
                        Transform enemyPos = hit.collider.gameObject.transform;

                        onClickEnviroment.Invoke(enemyPos.position);
                        Debug.Log("ENEMY");
                    }
                    onClickEnviroment.Invoke(hit.point);

                }
            }
        }
        else 
        {
            Cursor.SetCursor(pointer, Vector2.zero, CursorMode.Auto);
        }
    }
}

[System.Serializable]
public class EventVector3 : UnityEvent<Vector3> { }
